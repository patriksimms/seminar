import {Column, Entity, JoinColumn, OneToMany, OneToOne, PrimaryGeneratedColumn} from "typeorm";
import {Evaluation} from "../evaluation/evaluation.entity";

@Entity()
export class Product {

    constructor(url: string, competingProduct?: Product) {
        this.url = url;
        if (competingProduct) {
            this.competingProduct = competingProduct;
        }
    }

    @PrimaryGeneratedColumn()
    id: number;

    @Column({unique: true})
    url: string;

    @OneToMany(type => Evaluation, evaluation => evaluation.product, {})
    evaluations: Evaluation[];

    @Column({nullable: true, unique: true})
    title: string;

    @Column({type: 'decimal', precision: 5, scale: 2, nullable: true})
    price: number;

    @Column({nullable: true})
    brand: string;

    @Column({nullable: true})
    answeredQuestions: number

    @Column({type: 'decimal', precision: 5, scale: 2, nullable: true})
    avgRating: number;

    @Column({nullable: true})
    reviewCount: number;

    @Column({type: 'timestamp', nullable: true})
    lastCrawled: Date;

    @OneToOne(type => Product, {cascade: ['insert']})
    @JoinColumn()
    competingProduct: Product
}
