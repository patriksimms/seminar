import {forwardRef, Inject, Injectable, Logger} from "@nestjs/common";
import {Repository} from "typeorm";
import {Product} from "./product.entity";
import {InjectRepository} from "@nestjs/typeorm";
import {ScraperService} from "../evaluation/scraper.service";
import {ProductDto} from "../../dto/productDto";

@Injectable()
export class ProductService {
    constructor(
        @Inject(forwardRef(() => ScraperService))
        private readonly evaluationService: ScraperService,
        @InjectRepository(Product)
        private productRepository: Repository<Product>,
        private logger: Logger) {
    }

    async findAll(): Promise<Product[]> {
        return this.productRepository.find();
    }

    async persistProduct(productURL: string) {
        const newProduct = new Product(productURL);
        await this.productRepository.save(newProduct);
    }

    async crawlAllProducts(): Promise<number> {
        let products: [Product[], number] = await this.productRepository.createQueryBuilder()
            .getManyAndCount();
        const pDtos = await this.evaluationService.crawlEvaluationsForUrls(products[0].map(p => p.url))
        await this.updateProductsInformation(pDtos);

        return products[1]; // product count
    }

    // noinspection JSMethodCanBeStatic
    private updateProductsInformation(pDtos: ProductDto[]): Promise<void> {
        return Promise.all(pDtos).then((values) => {
            values.forEach(async (pDto) => {
                const product = await this.productRepository.findOne({where: {url: pDto.url}})
                await this.updateProductInformation(product, pDto);
                await this.evaluationService.persistReviews(pDto.topReviews, product);
            })
        })
    }

    private async updateProductInformation(product: Product, pDto: ProductDto) {
        product.title = pDto.title == '' ? null : pDto.title;
        product.price = +pDto.price;
        product.brand = pDto.brand;
        product.answeredQuestions = +pDto.answeredQuestions;
        product.avgRating = +pDto.avgRating;
        product.reviewCount = Number.isNaN(+pDto.reviewCount) ? 0 : +pDto.reviewCount;
        await this.productRepository.save(product);
    }

    async getProductsByBrand(brandString: string): Promise<Product[]> {
        return this.productRepository.find({
            where: `"brand" ILIKE '${brandString}%'`
        });
    }

    async persistProductsInDb(products: Product[]) {
        await this.productRepository.createQueryBuilder()
            .insert()
            .into(Product)
            .values(products)
            .orUpdate({conflict_target: ['url'], overwrite: ['title', 'brand']})
            .execute();
    }


    async deleteAllProducts(): Promise<number> {
        const productCount = await this.productRepository.count();
        await this.evaluationService.deleteAllEvaluations();
        await this.productRepository.delete({});
        return productCount;
    }
}
