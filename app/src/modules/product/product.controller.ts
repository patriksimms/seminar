import {Body, Controller, Delete, Get, HttpStatus, Param, Post, Query, Res} from "@nestjs/common";
import {ProductService} from "./product.service";
import {Response} from "express";
import {InjectRepository} from "@nestjs/typeorm";
import {Product} from "./product.entity";
import {Repository} from "typeorm";
import {ApiBody, ApiQuery, ApiResponse, ApiTags} from "@nestjs/swagger";
import {MessageDto} from "../../dto/messageDto";
import {ProductUrlDto} from "../../dto/productUrlDto";
import arrayContaining = jasmine.arrayContaining;
import {ProductDto} from "../../dto/productDto";

@ApiTags('Seminar')
@Controller()
export class ProductController {

    constructor(
        private readonly productService: ProductService,
        @InjectRepository(Product)
        private productRepository: Repository<Product>) {
    }

    @ApiResponse({
        status: HttpStatus.OK,
        description: 'all products currently registered',
        schema: {
            type: 'array',
            items: {
                type: 'object',
                properties: {
                    id: {
                        type: 'number',
                        example: 123
                    },
                    url: {
                        type: 'string',
                        example: 'https://www.amazon.de/Abfluss-Fee-100001-Duftsteine-4er-Set/dp/B004UJUX0Y/'
                    }
                }
            }
        }
    })
    @Post('/products')
    async getProducts(): Promise<any> {
        return {products: await this.productService.findAll()};
    }

    @ApiBody({
        type: ProductUrlDto
    })
    @ApiResponse({
        status: HttpStatus.CREATED,
        description: 'Product successfully persisted',
        type: MessageDto
    })
    @ApiResponse({
        status: HttpStatus.BAD_REQUEST,
        description: 'productURL is not defined',
        type: MessageDto
    })
    @Post('/addProduct')
    async addProduct(@Body() body: Body, @Res() res: Response): Promise<object> {
        const productURL = body['productURL'];
        if (undefined !== productURL && '' !== productURL) {
            await this.productService.persistProduct(productURL);
            return res.status(HttpStatus.CREATED).json({message: 'Product successfully persisted'});
        } else {
            return res.status(HttpStatus.BAD_REQUEST).json({message: 'productURL is not defined'});
        }
    }

    @ApiResponse({
        status: 200,
        description: 'Amount of products successfully crawled',
        type: MessageDto,
    })
    @Post('/crawlProductReviews')
    async crawlProductReviews(@Res() res: Response): Promise<object> {
        let crawledProducts = await this.productService.crawlAllProducts();
        return res.status(HttpStatus.OK).json({message: `${crawledProducts} products successfully crawled`});
    }

    @ApiQuery({
        name: 'brand',
        description: 'The brand to search for',
        example: 'DS-Produkte'
    })
    @ApiResponse({
        status: HttpStatus.OK,
        description: 'all products currently registered which matches the brand-string. The search is case-insensitive. All products match which starts with the search-term',
        type: [ProductDto]
    })
    @Get('/getProductsByBrand')
    async getProductsByBrand(@Query('brand') brandString: string, @Res() res: Response): Promise<object> {
        let products = await this.productService.getProductsByBrand(brandString);
        return res.status(HttpStatus.OK).json(products);
    }

    @ApiResponse({
        status: 200,
        description: 'Message containing the number of deleted products',
        schema: {
            type: 'string',
            example: '5 Products successfully deleted'
        }
    })
    @Delete('/deleteProducts')
    async deleteAllProducts(@Res() res: Response): Promise<object> {
        const deleteCount = await this.productService.deleteAllProducts();
        return res.status(HttpStatus.OK).json({message: `${deleteCount} Products deleted`});
    }
}
