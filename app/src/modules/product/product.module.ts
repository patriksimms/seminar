import {Logger, Module} from "@nestjs/common";
import {ProductController} from "./product.controller";
import {TypeOrmModule} from "@nestjs/typeorm";
import {Product} from "./product.entity";
import {ProductService} from "./product.service";
import {ScraperModule} from "../evaluation/scraper.module";

@Module({
    imports: [TypeOrmModule.forFeature([Product]), ScraperModule],
    controllers: [ProductController],
    providers: [ProductService, Logger],
    exports: [ProductService]
})
export class ProductModule {}
