import {Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn} from "typeorm";
import {Product} from "../product/product.entity";

@Entity()
export class Evaluation {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    title: string;

    @Column()
    rating: number;

    @Column({type: 'text', unique: true})
    text: string;

    @Column()
    verified: boolean;

    @Column()
    usefulAmount: number;

    @ManyToOne(type => Product, product => product.evaluations)
    product: Product;
}
