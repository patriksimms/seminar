export class ElementSelectors {
    public static TITLE = '#productTitle';
    public static PRICE = '#priceblock_ourprice';
    public static BRAND = '#bylineInfo';
    public static ANSWERED_QUESTIONS = '#askATFLink';
    public static AVG_RATING = "span[data-hook='rating-out-of-text']";
    public static REVIEW_COUNT = "div[data-hook='total-review-count']";

    public static REVEIW_SELECTOR = "div[data-hook='review']";

    public static REVIEW_TITLE = "a[data-hook='review-title']";
    public static REVIEW_ALTERNATIVE_TITLE = "span[data-hook='review-title']";
    public static REVIEW_TEXT = "div[data-hook='review-collapsed']";
    public static REVIEW_RATING = "i[data-hook='review-star-rating']";
    public static REVIEW_VERIFIED = "span[data-hook='avp-badge-linkless']";
    public static REVIEW_USEFUL_AMOUNT = "span[data-hook='helpful-vote-statement']";
    public static SELLING_RANK: '#productDetails_db_sections';

}
