import {Injectable, Logger} from "@nestjs/common";
import {InjectRepository} from "@nestjs/typeorm";
import {Repository} from "typeorm";
import {Evaluation} from "./evaluation.entity";
import {Product} from "../product/product.entity";
import {strict as assert} from 'assert';
import {ReviewDto} from "../../dto/reviewDto";
import {ProductDto} from "../../dto/productDto";
import {ElementSelectors} from "./elementSelectors";

@Injectable()
export class ScraperService {

    constructor(
        @InjectRepository(Evaluation)
        private evaluationRepository: Repository<Evaluation>,
        @InjectRepository(Product)
        private productRepository: Repository<Product>,
        private logger: Logger
    ) {
    }

    async crawlEvaluationsForUrls(productURLs: string[]): Promise<ProductDto[]> {
        let Crawler = require("crawler");
        const dtos = [];
        let finishCount = 0;
        const productCount = productURLs.length;

        let crawler = new Crawler({
            rateLimit: 5000,
            callback: (error, crawledHtml, done) => {
                if (error) {
                    this.logger.log(error);
                    return done();
                }
                const dto = this.fillProductDto(crawledHtml);
                finishCount++;
                dtos.push(dto);
                this.logger.log('Crawling of product done')
                done();
            }
        })

        productURLs.forEach(url => {
            crawler.queue({
                uri: encodeURI(url),
            });
        })

        while (productCount > finishCount) {
            await this.waitTimeOut()
        }

        assert(dtos.every(e => e.isValid));
        return dtos;
    }

    private fillProductDto(res): ProductDto {
        const $ = res.$
        const dto = new ProductDto();
        dto.url = decodeURIComponent(res.request.uri.href);
        dto.title = $(ElementSelectors.TITLE).text().trim();
        dto.price = $(ElementSelectors.PRICE).text();
        dto.brand = $(ElementSelectors.BRAND).text();
        dto.sellingRanks = $(ElementSelectors.SELLING_RANK).text().trim();
        dto.answeredQuestions = $(ElementSelectors.ANSWERED_QUESTIONS).text().trim();
        dto.avgRating = $(ElementSelectors.AVG_RATING).text();
        dto.reviewCount = $(ElementSelectors.REVIEW_COUNT).find('span').text().trim();
        dto.topReviews = ScraperService.extractReviews($);
        dto.ratingDistribution = ScraperService.extractRatingDistribution($);
        dto.lastCrawled = new Date().toLocaleString('de-DE', {timeZone: 'Europe/Berlin'});
        this.extractSellingRanks($, dto);
        dto.sanitize();
        return dto;
    }

    private extractSellingRanks($: CheerioAPI, dto: ProductDto): void {
        $('#productDetails_db_sections tr').each((idx, elem) => {
            if ('Amazon Bestseller-Rang' === $(elem).children().first().text().trim()) {
                const sellingRankString = $(elem).find('td').text().trim();
                dto.sellingRanks = sellingRankString.split('\n').filter(e => e !== '');
            }
        })
    }

    private static extractReviews($: CheerioAPI): ReviewDto[] {
        const reviews: ReviewDto[] = [];
        $(ElementSelectors.REVEIW_SELECTOR).each((index, review) => {
            const elem = new ReviewDto();
            elem.title = $(review).find(ElementSelectors.REVIEW_TITLE).text().trim();
            if ('' === elem.title) {
                elem.title = $(review).find(ElementSelectors.REVIEW_ALTERNATIVE_TITLE).text().trim();
            }
            elem.text = $(review).find(ElementSelectors.REVIEW_TEXT).text().trim();
            elem.reviewRating = $(review).find(ElementSelectors.REVIEW_RATING).text().trim();
            elem.verified = $(review).find(ElementSelectors.REVIEW_VERIFIED).text().trim();
            elem.usefulAmount = $(review).find(ElementSelectors.REVIEW_USEFUL_AMOUNT).text().trim();
            reviews.push(elem);
        });
        return reviews;
    }

    private static extractRatingDistribution($: CheerioAPI): string[] {
        const ratingDistribution: string[] = [];
        $('.a-histogram-row .a-meter').each((index, row) => {
            ratingDistribution.push($(row).attr('aria-label'))
        });
        return ratingDistribution;
    }

    async crawlEvaluationsForURL(url: string): Promise<ProductDto> {
        return (await this.crawlEvaluationsForUrls([url]))[0];
    }


    public async persistReviews(reviews: ReviewDto[], product: Product): Promise<boolean> {
        const results = reviews.map(review => this.persistOneReview(review, product));
        product.lastCrawled = new Date();
        await this.productRepository.save(product);
        return !results.includes(false);
    }

    // true if successful
    // currently synchronous, may need to be async later
    private persistOneReview(review: ReviewDto, product: Product): boolean {
        if (!review.isValid) {
            return false;
        }
        let ent = new Evaluation();
        ent.product = product;
        ent.rating = +review.reviewRating;
        ent.title = review.title;
        ent.text = review.text;
        ent.verified = 'true' === review.verified;
        ent.usefulAmount = +review.usefulAmount;
        this.evaluationRepository.createQueryBuilder()
            .insert()
            .into(Evaluation)
            .values(ent)
            .orUpdate({conflict_target: ['text'], overwrite: ['title']})
            .execute()
            .then();
        return true;

    }

    async deleteAllEvaluations(): Promise<number> {
        const deletedEvaluationsCount = await this.evaluationRepository.count();
        await this.evaluationRepository.delete({});
        return deletedEvaluationsCount
    }

    waitTimeOut() {
        return new Promise((resolve => setTimeout(resolve, 10)));
    }
}

