import {forwardRef, Logger, Module} from '@nestjs/common';
import {EvaluationController} from './evaluation.controller';
import {ScraperService} from "./scraper.service";
import {TypeOrmModule} from "@nestjs/typeorm";
import {Evaluation} from "./evaluation.entity";
import {ProductModule} from "../product/product.module";
import {Product} from "../product/product.entity";

@Module({
    imports: [forwardRef(() => ProductModule), TypeOrmModule.forFeature([Product, Evaluation])],
    controllers: [EvaluationController],
    providers: [ScraperService, Logger],
    exports: [ScraperService]

})
export class ScraperModule {
}
