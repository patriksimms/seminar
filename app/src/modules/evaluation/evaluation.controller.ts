import {Body, Controller, Delete, HttpStatus, Post, Res} from '@nestjs/common';
import {ProductService} from "../product/product.service";
import {ScraperService} from "./scraper.service";
import {Response} from "express";
import {ApiBody, ApiResponse, ApiTags} from "@nestjs/swagger";
import {ProductUrlDto} from "../../dto/productUrlDto";
import {MessageDto} from "../../dto/messageDto";
import {ProductDto} from "../../dto/productDto";

@ApiTags('Seminar')
@Controller()
export class EvaluationController {

    constructor(
        private readonly productService: ProductService,
        private readonly evaluationService: ScraperService) {
    }

    @ApiBody({
        type: ProductUrlDto
    })
    @ApiResponse({
        status: HttpStatus.BAD_REQUEST,
        type: MessageDto
    })
    @ApiResponse({
        status: HttpStatus.OK,
        description: 'The crawled product',
        type: ProductDto
    })
    @Post('/crawlProductURL')
    async crawlProductURL(@Body() body: Body, @Res() res: Response): Promise<object> {
        const productURL = body['productURL'];
        if (undefined !== productURL && '' !== productURL) {
            const productDto = (await this.evaluationService.crawlEvaluationsForUrls([productURL]))[0];
            return res.status(HttpStatus.OK).json(productDto);
        } else {
            return res.status(HttpStatus.BAD_REQUEST).json({message: 'productURL is not defined'}).send();
        }
    }

    @ApiResponse({
        status: 200,
        description: 'Message containing the number of deleted evaluations',
        schema: {
            type: 'string',
            example: '5 products successfully deleted'
        }
    })
    @Delete('/deleteEvaluations')
    async deleteAllEvaluations(@Res() res: Response): Promise<object> {
        const deleteCount = await this.evaluationService.deleteAllEvaluations();
        return res.status(HttpStatus.OK).json({message: `${deleteCount} Evaluations deleted`});
    }
}
