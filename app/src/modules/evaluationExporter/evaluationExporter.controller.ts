import {Controller, Get, HttpStatus, Res} from "@nestjs/common";
import {Response} from "express";
import {EvaluationExporterService} from "./evaluationExporter.service";
import {ApiProduces, ApiResponse, ApiTags} from "@nestjs/swagger";

@ApiTags('Export')
@Controller()
export class EvaluationExporterController {


    constructor(private readonly evaluationsExporterService: EvaluationExporterService) {
    }

    @ApiProduces('text/csv')
    @ApiResponse({
        status: HttpStatus.OK,
        type: String,
        description: 'The csv containing all evaluations',
    })
    @Get('/exportAllEvaluations')
    async getAllEvaluations(@Res() res: Response) {
        const stream = await this.evaluationsExporterService.createExportCSV();
        res.set({
            'Content-Type': 'text/csv',
        })
        return stream.pipe(res);
    }
}
