import {InjectRepository} from "@nestjs/typeorm";
import {Evaluation} from "../evaluation/evaluation.entity";
import {Repository} from "typeorm";
import {createObjectCsvWriter} from "csv-writer";
import {createReadStream, ReadStream} from 'fs';

export class EvaluationExporterService {


    constructor(@InjectRepository(Evaluation)
                private evaluationRepository: Repository<Evaluation>) {
    }

    async createExportCSV(): Promise<ReadStream> {
        let evaluations = await this.evaluationRepository
            .createQueryBuilder('evaluation')
            .leftJoinAndSelect('evaluation.product', 'product')
            .getMany();
        const res = evaluations as Array<any>;
        res.map((obj) => {
            obj.productID = obj.product.id;
            obj.product = obj.product.title;
        })

        const csvWriter = createObjectCsvWriter({
            path: 'files/testRes.csv',
            header: [
                {id: 'id', title: 'reviewID'},
                {id: 'title', title: 'title'},
                {id: 'rating', title: 'rating'},
                {id: 'text', title: 'text'},
                {id: 'verified', title: 'verified'},
                {id: 'usefulAmount', title: 'usefulAmount'},
                {id: 'product', title: 'productName'},
                {id: 'productID', title: 'productID'}
            ]
        })
        await csvWriter.writeRecords(res);

        return createReadStream('files/testRes.csv');
    }
}
