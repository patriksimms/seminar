import {Module} from "@nestjs/common";
import {EvaluationExporterController} from "./evaluationExporter.controller";
import {EvaluationExporterService} from "./evaluationExporter.service";
import {ScraperModule} from "../evaluation/scraper.module";
import {TypeOrmModule} from "@nestjs/typeorm";
import {Evaluation} from "../evaluation/evaluation.entity";

@Module({
    imports: [TypeOrmModule.forFeature([Evaluation]), ScraperModule],
    controllers: [EvaluationExporterController],
    providers: [EvaluationExporterService]
})

export class EvaluationExporter {
}
