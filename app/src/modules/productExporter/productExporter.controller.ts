import {Controller, Get, HttpStatus, Res} from "@nestjs/common";
import {ApiProduces, ApiResponse, ApiTags} from "@nestjs/swagger";
import {Response} from "express";
import {ProductExporterService} from "./productExporter.service";


@ApiTags('Export')
@Controller()
export class ProductExporterController {


    constructor(private readonly productExporterService: ProductExporterService) {
    }

    @ApiProduces('text/csv')
    @ApiResponse({
        status: HttpStatus.OK,
        type: String,
        description: 'The csv containing all products'
    })
    @Get('/exportAllProducts')
    async createExportCSV(@Res() res: Response) {
        const stream = await this.productExporterService.createExportCSV();
        res.set({
            'Content-Type': 'text/csv',
        })
        return stream.pipe(res);
    }
}
