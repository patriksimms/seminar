import {InjectRepository} from "@nestjs/typeorm";
import {Product} from "../product/product.entity";
import {Repository} from "typeorm";
import {createReadStream, ReadStream} from "fs";
import { createObjectCsvWriter } from "csv-writer";

export class ProductExporterService {


    constructor(@InjectRepository(Product)
                private productRepository: Repository<Product>) {
    }

    async createExportCSV(): Promise<ReadStream> {
        let products = await this.productRepository.find();

        const csvWriter = createObjectCsvWriter({
            path: 'files/testRes2.csv',
            header: [
                {id: 'id', title: 'id'},
                {id: 'url', title: 'productURL'},
                {id: 'title', title: 'title'},
                {id: 'price', title: 'price'},
                {id: 'brand', title: 'brand'},
                {id: 'answeredQuestions', title: 'answeredQuestions'},
                {id: 'avgRating', title: 'avgRating'},
                {id: 'reviewCount', title: 'reviewCount'},
                {id: 'lastCrawled', title: 'lastCrawled', }
            ]
        });

        await csvWriter.writeRecords(products);

        return createReadStream('files/testRes2.csv');
    }
}
