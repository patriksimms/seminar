import {Module} from "@nestjs/common";
import {ProductExporterController} from "./productExporter.controller";
import {ProductExporterService} from "./productExporter.service";
import {TypeOrmModule} from "@nestjs/typeorm";
import {Product} from "../product/product.entity";

@Module({
    imports: [TypeOrmModule.forFeature([Product])],
    controllers: [ProductExporterController],
    providers: [ProductExporterService],
})
export class ProductExporterModule {}
