import {Logger, Module} from "@nestjs/common";
import {ProductImporterService} from "./productImporter.service";
import {TypeOrmModule} from "@nestjs/typeorm";
import {Product} from "../product/product.entity";
import {ProductImporterController} from "./productImporter.controller";
import {ProductService} from "../product/product.service";
import {ProductModule} from "../product/product.module";

@Module({
    imports: [TypeOrmModule.forFeature([Product]), ProductModule],
    controllers: [ProductImporterController],
    providers: [ProductImporterService, Logger]
})
export class ProductImporter {}
