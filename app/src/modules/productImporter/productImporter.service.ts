import {Injectable, Logger} from "@nestjs/common";
import * as fs from 'fs';
import * as csv from "csv-parser"
import {Product} from "../product/product.entity";
import {InjectRepository} from "@nestjs/typeorm";
import {Repository} from "typeorm";
import {ProductService} from "../product/product.service";

@Injectable()
export class ProductImporterService {

    constructor(@InjectRepository(Product)
                private productRepository: Repository<Product>,
                private productService: ProductService,
                private logger: Logger) {
    }

    async importProductCSVtoDB(filename: string) {
        let results = [];
        fs.createReadStream(`files/${filename}`)
            .pipe(csv({separator: ';', headers: ['brand', 'url']}))
            .on('data', (data) => {
                results.push(data);
            })
            .on('end', () => {
                this.persistProductList(results);
                fs.unlink(`files/${filename}`, (err) => {
                    if (err) {
                        this.logger.error(err);
                    }
                })
                this.logger.log(`${results.length} products from CSV successfully parsed and persisted in db`)
            })
    }

    private async persistProductList(results: any[]): Promise<void> {
        let competingProductsExist = false;
        let competingProducts = null;
        const competingProductKey = Object.keys(results[0])[2];
        const urlKey = Object.keys(results[0])[1];


        if (Object.keys(results[0]).length > 2) {
            competingProducts = results.map(e => new Product(e[competingProductKey]));
            await this.productService.persistProductsInDb(competingProducts);
            competingProductsExist = true;
        }
        for (const res of results) {
            let idx = results.indexOf(res);
            let product = null;
            if (competingProductsExist) {
                product = new Product(res[urlKey], await this.getCompetingProductByURL(competingProducts[idx]));
            } else {
                product = new Product(res[urlKey]);
            }
            await this.productService.persistProductsInDb([product]);
        }
    }

    private getCompetingProductByURL(competingProduct: Product) {
        return this.productRepository.findOne({where: {url: competingProduct.url}});
    }

}
