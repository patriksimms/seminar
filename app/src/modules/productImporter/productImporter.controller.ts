import {Controller, HttpStatus, Post, Res, UploadedFile, UseInterceptors} from "@nestjs/common";
import {ProductImporterService} from "./productImporter.service";
import {Response} from "express";
import {ApiBody, ApiConsumes, ApiResponse, ApiTags} from "@nestjs/swagger";
import {MessageDto} from "../../dto/messageDto";
import {diskStorage} from 'multer';
import {FileInterceptor} from "@nestjs/platform-express";
import {csvFileFilter, editFileName} from "../../fileoperations";

@ApiTags('Import')
@Controller()
export class ProductImporterController {

    constructor(private readonly productImporterService: ProductImporterService) {
    }

    @ApiConsumes('multipart/form-data')
    @ApiBody({
        schema: {
            type: 'object',
            properties: {
                file: {
                    description: 'a csv file containing the products',
                    type: 'string',
                    format: 'binary',
                },
            },
        },
    })
    @ApiResponse({
        status: HttpStatus.ACCEPTED,
        description: 'CSV successfully parsed and persisted in db',
        type: MessageDto
    })
    @Post('/importCSV')
    @UseInterceptors(FileInterceptor('file', {
        fileFilter: csvFileFilter,
        storage: diskStorage({
            destination: './files',
            filename: editFileName
        }),
    }))
    async importProductCSV(@UploadedFile() file, @Res() res: Response) {

        await this.productImporterService.importProductCSVtoDB(file.filename);
        return res.status(HttpStatus.ACCEPTED).json({message: 'CSV successfully parsed and persisted in db'})
    }
}

