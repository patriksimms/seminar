import {Module} from '@nestjs/common';
import {AppController} from './app.controller';
import {TypeOrmModule} from "@nestjs/typeorm";
import {ProductModule} from "./modules/product/product.module";
import {ScraperModule} from "./modules/evaluation/scraper.module";
import {ProductImporter} from "./modules/productImporter/productImporter.module";
import {MulterModule} from "@nestjs/platform-express";
import {EvaluationExporter} from "./modules/evaluationExporter/evaluationExporter.module";
import {ProductExporterModule} from "./modules/productExporter/productExporter.module";

@Module({
    imports: [
        TypeOrmModule.forRoot(),
        ProductModule,
        ScraperModule,
        ProductImporter,
        EvaluationExporter,
        ProductExporterModule,
        MulterModule.register(
            {
                dest: './files'
            }
        )
    ],
    controllers: [AppController]
})
export class AppModule {
}
