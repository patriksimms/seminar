import {ApiProperty} from "@nestjs/swagger";

export class ReviewDto {

    @ApiProperty({
        description: 'The title of the review',
        example: 'Sauberer Abfluss'
    })
    title: string;

    text: string;

    reviewRating: string;

    isValid: boolean = false;

    usefulAmount: string;

    verified: string;

    sanitize() {
        this.reviewRating = ReviewDto.defaultForNull(this.reviewRating.match(/^[0-9],[0-9] von 5 Sternen$/gi))
            .replace(' von 5 Sternen', '').replace(/,/g, '.');
        this.usefulAmount = ReviewDto.defaultForNull(this.usefulAmount.match(/[0-9]* Personen fanden diese Informationen hilfreich$/gi)).replace(' Personen fanden diese Informationen hilfreich', '')

        if ('Verifizierter Kauf' === this.verified) {
            this.verified = 'true';
        } else {
            this.verified = 'false';
        }
        this.isValid = true;
    }

    private static defaultForNull(arr: any[]): string {
        return arr === null ? '' : arr[0];
    }

}
