import {ReviewDto} from "./reviewDto";
import {ApiExtraModels, ApiProperty} from "@nestjs/swagger";

export class ProductDto {

    @ApiProperty({
        description: 'The title of the product',
        example: 'Abflussfee 4er Nachfüll-Set für Abflussfee Waschbeckenstöpsel | Reinigungssteine für Abflüsse ' +
            'jeder Art, Abflussreiniger | inkl. Frische Duft [Apfel Aroma, grün]'
    })
    title: string;

    @ApiProperty({
        description: 'The amazon URL of the product',
        example: 'https://www.amazon.de/Abfluss-Fee-100001-Duftsteine-4er-Set/dp/B004UJUX0Y/ref=sr_1_5?__mk_de_' +
            'DE=%C3%85M%C3%85%C5%BD%C3%95%C3%91&dchild=1&keywords=Abflussfee&qid=1601031748&sr=8-5'
    })
    url: string;

    @ApiProperty({
        description: 'the decimal price in euros. If the price is empty, the product is probably out of stock ',
        example: '2,99€'
    })
    price: string;

    @ApiProperty({
        description: 'The brand fo the product',
        example: 'DS- Produkte'
    })
    brand: string;

    @ApiProperty({
        description: 'The amount of answered questions for the prodcut',
        example: '1'
    })
    answeredQuestions: string;

    @ApiProperty({
        description: 'The average rating of the product',
        example: '4.4'
    })
    avgRating: string;

    @ApiProperty({
        description: 'The ampount of how many reviews this product has',
        example: '651'
    })
    reviewCount: string;

    @ApiProperty({
        description: 'The distribution of the reviews starting with 5*',
        example: [
            '63%',
            '19%',
            '12%',
            '3%',
            '2%'
        ]
    })
    ratingDistribution: string[];

    @ApiProperty({
        description: 'The Top Reviews for the Product',
        example: [
            {
                "isValid": true,
                "title": "Sauberer Abfluss",
                "text": "Der Abfluss beibt auber. Wir haben diesen Duftstein seit Anbeginn.Nach ungefähr 3 Wochen ist der Stein verbraucht und muss erneuert werden.",
                "reviewRating": "5.0",
                "verified": "true",
                "usefulAmount": ""
            },
            {
                "isValid": true,
                "title": "Unangenehmer Geruch",
                "text": "Die Duft?Steine haben einen unangenehmen chemischen Geruch. Darüberhinaus halten sie weniger als 10 Tage.",
                "reviewRating": "1.0",
                "verified": "true",
                "usefulAmount": "5"
            },
        ]
    })
    topReviews: ReviewDto[];

    @ApiProperty({
        description: 'The TimeStamp the last time the product was crawled. The format is adjusted to the timezone Europe/ Berlin and in de-DE format',
        type: String,
        example: '15.11.2020, 17:36:42'
    })
    lastCrawled: string;

    @ApiProperty({
        description: 'The Selling Ranks for the product',
        type: 'array',
        example: ['Nr. 3,417 in Küche, Haushalt & Wohnen (Siehe Top 100 in Küche, Haushalt & Wohnen)', 'Nr. 1 in Bügeleisensysteme']
    })
    sellingRanks: any;

    isValid: boolean = false;

    sanitize() {
        this.url = ProductDto.defaultForNull(this.url.match(/(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})/gi));

        // removing no-break spaces
        this.price = this.price.replace(/ €/gi, '');
        this.price = ProductDto.defaultForNull(this.price.match(/^[0-9]*,[0-9][0-9]$/gi)).replace(',', '.')
        this.brand = this.brand.replace(/Marke: /gi, '');
        this.answeredQuestions = this.answeredQuestions.replace(/ beantwortete Fragen/gi, '')
        this.avgRating = this.avgRating.replace(/ von 5/gi, '').replace(',', '.');
        this.reviewCount = this.reviewCount.replace(/ globale Bewertungen/gi, '').replace('.', '');
        this.ratingDistribution = this.ratingDistribution.map(value => value = ProductDto.defaultForNull(value.match(/^[0-9]?[0-9]%$/gi)));

        this.topReviews.forEach(r => r.sanitize())
        this.isValid = this.topReviews.every(r => r.isValid);
    }

    private static defaultForNull(arr: any[]): string {
        return arr === null ? '' : arr[0];
    }
}
