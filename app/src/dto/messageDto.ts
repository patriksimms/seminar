import {ApiProperty} from "@nestjs/swagger";

export class MessageDto {

    @ApiProperty({
        description: 'a response message specific to the endpoint',
        example: 'Product successfuly persisted'
    })
    message: string;
}
