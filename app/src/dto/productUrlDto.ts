import {ApiProperty} from "@nestjs/swagger";

export class ProductUrlDto {

    @ApiProperty({
        description: 'a amazon product url',
        example: 'https://www.amazon.de/Abfluss-Fee-100001-Duftsteine-4er-Set/dp/B004UJUX0Y/'
    })
    productURL: string;
}
