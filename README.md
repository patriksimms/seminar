<p align="center">
  <a href="https://www.fh-wedel.de/" target="blank"><img src="https://www.fh-wedel.de/typo3conf/ext/in2template/Resources/Public/Images/Frontend/Logo_Fachhochschule-Wedel.svg" height="70" alt="Nest Logo" /></a>
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo_text.svg" height="70" alt="Nest Logo" /></a>
  <a href="https://www.postgresql.org/" target="blank"><img src="https://upload.wikimedia.org/wikipedia/de/thumb/4/4b/Postgresql.svg/540px-Postgresql.png" height="70" alt="Nest Logo" /></a>
  <a href="https://www.docker.com/" target="blank"><img src="https://www.docker.com/sites/default/files/d8/2019-07/horizontal-logo-monochromatic-white.png" height="70" alt="Nest Logo" /></a>
</p>

# Seminar WS 2020 Patrik Simms


# Description
This application implements a [RESTful](https://de.wikipedia.org/wiki/Representational_State_Transfer) HTTP API to crawl reviews of products listed on amazon.com

**The application uses following tools:**

[nestJS](https://nestjs.com/), a progressive node.js framework which is haevily inspired by angular. This is used for the application logic and providing the HTTP API interface

[postgreSQL](https://www.postgresql.org/), a database for storing the data for the reviews/ products

[docker](https://www.docker.com/), a software for "container virtualization". This allows the application to run on every device which has docker installed the same way without any further installation

# Usage

This application does not have any frontend, but it can be used via the interactive REST Client OpenAPI provides. It can be accessed at
[localhost:8030/api](http://localhost:8030/api)

### Prerequisites
- docker ([Get Docker](https://docs.docker.com/get-docker/))
- an internet connection
- (a persistent [volume mount](https://docs.docker.com/storage/volumes/) for storing the crawling results over multiple sessions of use). Note for windows users [here](https://docs.docker.com/docker-for-windows/wsl/)

### Start an development server in attached mode
````sh
$ .docker/dev-server.sh
````
### Start an development server in detached mode
````sh
$ .docker/dev-server.sh -d
````
The API is accessible at [localhost:8030/api](http://localhost:8030/api)


### API
for OpenAPI Documentation, see [openapi](openapi/openapi.json) directory

### Tests
Soon :wink:
