FROM node:14.9-alpine

COPY app /app

WORKDIR /app

RUN npm install -g @nestjs/cli
RUN npm install

CMD [ "nest", "start:prod" ]

EXPOSE 8030
